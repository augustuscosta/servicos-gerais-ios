//
//  ONRequestFactory.swift
//  ServicosGerais
//
//  Created by Augustus Costa on 17/11/14.
//  Copyright (c) 2014 Augustus Costa. All rights reserved.
//

import Foundation

class ONRequestFactory: NSObject{
    
    func createSessionRequest(email:String, password:String, notificationId:String!)->ONRequest{
        let request = ONRequest()
        request.path = "sessions"
        
        request.addParam("session[email]", value: email)
        request.addParam("session[password]", value: password)
        if(notificationId != nil){
            request.addParam("session[token]", value: notificationId)
        }
        
        request.addParam("session[os]", value: "IOS")
        
        return request
    }
    
    func createUsuarioRequest(nome:String, telefone:String, email:String, password:String)->ONRequest{
        let request = ONRequest()
        request.path = "usuarios"
        
        request.addParam("usuario[nome]", value: nome)
        request.addParam("usuario[telefone]", value: telefone)
        request.addParam("usuario[email]", value: email)
        request.addParam("usuario[password]", value: password)
        
        return request
    }

    
    func deleteSessionRequest(sessionId:NSNumber)->ONRequest{
        
        let request = ONRequest()
        
        request.path = "sessions/" + sessionId.stringValue
        
        return request
    }
    
}