//
//  SigninViewController.swift
//  ServicosGerais
//
//  Created by Augustus Costa on 12/11/14.
//  Copyright (c) 2014 Augustus Costa. All rights reserved.
//

import UIKit

class SignupViewController: FXFormViewController {
    
    override func awakeFromNib() {
        
        formController.form = ONCreateUserForm()
    }
    
    func criarContaForm(cell: FXFormFieldCellProtocol) {
        let form = cell.field.form as ONCreateUserForm
        
        
        let erros : NSArray = form.isValid()
        
        if(erros.count > 0){
            var message : String = ""
            for (erro) in erros{
                message = message.stringByAppendingString(erro as String)
                message = message.stringByAppendingString("")
                UIAlertView(title: "Erro na validação dos campos", message: (erro as String), delegate: nil, cancelButtonTitle: "OK").show()
                return
            }
        }
        
        if form.agreedToTerms {
            
            if(form.profilePhoto == nil){
                signIn(form)
            }else{
                signInWitImage(form)
            }
            
        } else {
            
            UIAlertView(title: "Termos e condições", message: "Você deve aceitar os termos e condições para proseguir com o cadastro", delegate: nil, cancelButtonTitle: "OK").show()
        }
    }
    
    func criarContaComFacebookForm(cell: FXFormFieldCellProtocol) {
        let form = cell.field.form as ONCreateUserForm
        
        if form.agreedToTerms {
            
            //FACEBOOK THINGS
            
        } else {
            
            UIAlertView(title: "Termos e condições", message: "Você deve aceitar os termos e condições para proseguir com o cadastro", delegate: nil, cancelButtonTitle: "OK").show()
        }
    }
    
    
    func signIn(form:ONCreateUserForm){
        signIn(form.name!, telefone: form.phone, email: form.email, senha: form.password)
    }
    

    
    func signIn(nome:NSString!, telefone:NSString!, email:NSString!, senha:NSString!){
        let request:ONRequest = ONRequestFactory().createUsuarioRequest(nome, telefone: telefone, email: email, password: senha)
        
       RKObjectManager.sharedManager().postObject(nil, path:request.path, parameters:request.params,
            success:{ operation, mappingResult in
                self.login(email, senha: senha)
            },
            failure:{ operation, error in
                println("SiginIn request failed with error: " + error!.localizedDescription)
                var erroMessage : NSString = error!.localizedDescription
                
                if(error != nil){
                    if(error.localizedRecoverySuggestion != nil){
                        erroMessage = ONRailsError().getError(error!.localizedRecoverySuggestion!)
                    }
                    
                }
                
                let alertController = UIAlertController(title: "Não foi possível criar o usuário", message:erroMessage, preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Voltar", style: UIAlertActionStyle.Default,handler: nil))
                
                self.presentViewController(alertController, animated: true, completion: nil)
                
            }
        )
        
    }
    
    func signInWitImage(form:ONCreateUserForm){
        signInWitImage(form.name, telefone: form.phone, email: form.email, senha: form.password, image: form.profilePhoto)
    }
    
    func signInWitImage(nome:NSString!, telefone:NSString!, email:NSString!, senha:NSString!, image:UIImage!){
        let request:ONRequest = ONRequestFactory().createUsuarioRequest(nome, telefone: telefone, email: email, password: senha)
        
       let mutableRequest : NSMutableURLRequest = RKObjectManager.sharedManager().multipartFormRequestWithObject(nil, method: RKRequestMethod.POST, path: request.path, parameters: request.params, constructingBodyWithBlock: {data in
            var formData:AFMultipartFormData = data as AFMultipartFormData
            formData.appendPartWithFileData(UIImagePNGRepresentation(image),
                name: "usuario[avatar]",
                fileName: nome.stringByAppendingString("avatar.png"),
                mimeType: "image/png")
        })
        
        
        let operation : RKObjectRequestOperation = RKObjectManager.sharedManager().managedObjectRequestOperationWithRequest(mutableRequest, managedObjectContext : RKManagedObjectStore.defaultStore().persistentStoreManagedObjectContext ,success:{ operation, mappingResult in
            self.login(email, senha: senha)
            },
            failure:{ operation, error in
                println("SiginIn request failed with error: " + error!.localizedDescription)
                var erroMessage : NSString = error!.localizedDescription
                
                if(error != nil){
                    if(error.localizedRecoverySuggestion != nil){
                        erroMessage = ONRailsError().getError(error!.localizedRecoverySuggestion!)
                    }
                    
                }
                
                let alertController = UIAlertController(title: "Não foi possível criar o usuário", message:erroMessage, preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Voltar", style: UIAlertActionStyle.Default,handler: nil))
                
                self.presentViewController(alertController, animated: true, completion: nil)
        })

        RKObjectManager.sharedManager().enqueueObjectRequestOperation(operation)
    }
    
    func login(email:NSString, senha:NSString){
        let request:ONRequest = ONRequestFactory().createSessionRequest(email, password: senha, notificationId: NSUserDefaults.standardUserDefaults().stringForKey("token"))
        
        RKObjectManager.sharedManager().postObject(nil, path:request.path, parameters:request.params,
            success:{ operation, mappingResult in
                self.dismiss()
            },
            failure:{ operation, error in
               self.dismiss()
            }
        )
        
    }
    
    func dismiss(){
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    
}
