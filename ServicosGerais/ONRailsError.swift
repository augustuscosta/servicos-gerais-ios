//
//  ONErrorCollection.swift
//  ServicosGerais
//
//  Created by Augustus Costa on 21/01/15.
//  Copyright (c) 2015 Augustus Costa. All rights reserved.
//

import Foundation

class ONRailsError: NSObject {

    var errors:NSMutableDictionary!
    var errorMessage:NSString!
    
    
    func getError(json:NSString)->NSString{
        return getError(json.dataUsingEncoding(NSUTF8StringEncoding)!)
    }
    
    
    func getError(json:NSData)->NSString{
        var parseError: NSError?
        let parsedObject: AnyObject? = NSJSONSerialization.JSONObjectWithData(json,
            options: NSJSONReadingOptions.AllowFragments,
            error:&parseError)
        
        let dictionary = parsedObject as NSDictionary
        
        return getError(dictionary)
        
    }
    
    func getError(dictionary : NSDictionary)->NSString{
        var toReturn : NSString = ""
        var fieldString :NSString!
        var errorString :NSString!
        var errorArray : NSArray!
        for (field, error) in dictionary {
            fieldString = field.description
            if(toReturn.length >= 1){
                toReturn = toReturn.stringByAppendingString(" ")
            }
            toReturn = toReturn.stringByAppendingString(fieldString)
            toReturn = toReturn.stringByAppendingString(": ")
            errorArray = error as NSArray
            errorString = ""
            for(item) in errorArray{
                if(errorString.length >= 1){
                    errorString = errorString.stringByAppendingString(", ")
                }
                errorString = errorString.stringByAppendingString(item.description)
            }
            toReturn = toReturn.stringByAppendingString(errorString)
        }
        
        return toReturn
    }
    
   
}
