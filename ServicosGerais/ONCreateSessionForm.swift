//
//  ONCreateSessionForm.swift
//  ServicosGerais
//
//  Created by Augustus Costa on 28/01/15.
//  Copyright (c) 2015 Augustus Costa. All rights reserved.
//

import UIKit


class ONCreateSessionForm: NSObject, FXForm{
    
    var email: String?
    var password: String?
    
    
    func fields() -> NSArray {
        
        return [
            
            [FXFormFieldTitle: "Entrar com Facebook", FXFormFieldHeader: "", FXFormFieldAction: "entrarComFacebookForm:"],
            
            [FXFormFieldKey: "email", FXFormFieldHeader: "Entrar", FXFormFieldTitle: "Email"],
            [FXFormFieldKey: "password", FXFormFieldTitle: "Senha"],
            
            [FXFormFieldTitle: "Entrar", FXFormFieldAction: "entrarForm:"],
            
            [FXFormFieldTitle: "Criar conta", FXFormFieldHeader: "", FXFormFieldAction: "criarContaFormForm:"]
            
            
            
        ]
    }
    
    func isValid() -> NSMutableArray{
        var errorMessages:NSMutableArray = []

        if(isNilOrEmpty(email)){
            errorMessages.addObject("O campo email deve ser preenchido.")
        }else if(!isValidEmail(email!)){
            errorMessages.addObject("O campo email deve ser preenchido corretamente.")
        }

        if(isNilOrEmpty(password)){
            errorMessages.addObject("O campo senha deve ser preenchido.")
        }
        
        
        return errorMessages
    }
    
    func isValidEmail(testStr:String) -> Bool {
        println("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        var emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest?.evaluateWithObject(testStr)
        return result!
    }

}
