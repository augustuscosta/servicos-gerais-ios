//
//  ONUsuario.h
//  ServicosGerais
//
//  Created by Augustus Costa on 26/01/15.
//  Copyright (c) 2015 Augustus Costa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ONUsuario : NSManagedObject

@property (nonatomic, retain) NSString * avatar_url;
@property (nonatomic, retain) NSString * avatar_url_medium;
@property (nonatomic, retain) NSString * avatar_url_thumb;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * facebook_avatar_url;
@property (nonatomic, retain) NSNumber * identifier;
@property (nonatomic, retain) NSString * nome;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * telefone;

@end
