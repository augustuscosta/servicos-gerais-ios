//
//  TermosViewController.swift
//  ServicosGerais
//
//  Created by Augustus Costa on 20/01/15.
//  Copyright (c) 2015 Augustus Costa. All rights reserved.
//

import UIKit

class TermosViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebView()
    }
    
    func loadWebView(){
        let url = NSURL(string: "http://google.com.br")
        let request = NSURLRequest(URL: url!)
        webView.loadRequest(request)
    }
    
}