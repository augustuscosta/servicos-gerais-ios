//
//  ONSession.m
//  ServicosGerais
//
//  Created by Augustus Costa on 11/11/14.
//  Copyright (c) 2014 Augustus Costa. All rights reserved.
//

#import "ONSession.h"


@implementation ONSession

@dynamic email;
@dynamic password;
@dynamic identifier;

@end
