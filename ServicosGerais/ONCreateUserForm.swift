//
//  ONCreateUserForm.swift
//  ServicosGerais
//
//  Created by Augustus Costa on 27/01/15.
//  Copyright (c) 2015 Augustus Costa. All rights reserved.
//

import UIKit


class ONCreateUserForm: NSObject, FXForm{

    
    var name: String?
    var email: String?
    var password: String?
    var phone: String?
    var repeatPassword: String?
    var profilePhoto: UIImage?
    
    var agreedToTerms = false
    
    func fields() -> NSArray {
        
        return [
            
            [FXFormFieldTitle: "Criar conta com Facebook", FXFormFieldHeader: "", FXFormFieldAction: "criarContaComFacebookForm:"],
            
            [FXFormFieldKey: "name", FXFormFieldHeader: "Criar conta", FXFormFieldTitle: "Nome"],
            [FXFormFieldKey: "email", FXFormFieldTitle: "Email"],
            [FXFormFieldKey: "phone", FXFormFieldTitle: "Telefone"],
            [FXFormFieldKey: "password", FXFormFieldTitle: "Senha"],
            [FXFormFieldKey: "repeatPassword", FXFormFieldTitle: "Repetir senha"],
            [FXFormFieldKey: "profilePhoto", FXFormFieldTitle: "Imagem"],
            
            [FXFormFieldHeader: "Legal",
                FXFormFieldTitle: "Termos e condições",
                FXFormFieldSegue: "termosSegue"],
            
            [FXFormFieldTitle: "Política de privacidade",
                FXFormFieldSegue: "politicaSegue"],
            
            [FXFormFieldKey: "agreedToTerms", FXFormFieldTitle: "Eu concordo com os termos", FXFormFieldType: FXFormFieldTypeOption],

            
            [FXFormFieldTitle: "Criar conta", FXFormFieldHeader: "", FXFormFieldAction: "criarContaForm:"]
            
        ]
    }
    
    func isValid() -> NSMutableArray{
        var errorMessages:NSMutableArray = []
        
        if(isNilOrEmpty(name)){
            errorMessages.addObject("O campo nome deve ser preenchido.")
        }
        if(isNilOrEmpty(email)){
            errorMessages.addObject("O campo email deve ser preenchido.")
        }else if(!isValidEmail(email!)){
            errorMessages.addObject("O campo email deve ser preenchido corretamente.")
        }
        
        if(isNilOrEmpty(phone)){
            errorMessages.addObject("O campo telefone deve ser preenchido.")
        }
        
        if(isNilOrEmpty(password)){
            errorMessages.addObject("O campo senha deve ser preenchido.")
        }
        
        if(isNilOrEmpty(repeatPassword)){
            errorMessages.addObject("O campo repetir senha deve ser preenchido.")
        }
        
        if(password != repeatPassword){
            errorMessages.addObject("Os campos senha e repetir senha não conferem.")
        }
        
        
        return errorMessages
    }
    
    func isValidEmail(testStr:String) -> Bool {
        println("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        
        var emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest?.evaluateWithObject(testStr)
        return result!
    }
    

}