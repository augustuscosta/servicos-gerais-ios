//
//  ONResponseDescriptor.swift
//  ServicosGerais
//
//  Created by Augustus Costa on 11/11/14.
//  Copyright (c) 2014 Augustus Costa. All rights reserved.
//

import UIKit

class ONResponseDescriptor: NSObject {
    
    class func createSessionDescriptor() -> RKResponseDescriptor {
            
        let mapping = ONMapping.createSessionMapping()
        let method = RKRequestMethod.POST
        let pathPattern = "sessions"
        let keyPath: NSString! = nil
        let statusCode = RKStatusCodeIndexSetForClass(UInt(201))
    
        let responseDescriptor = responseDescriptorWithMapping(mapping, withMethod: method, withPathPattern: pathPattern, withKeyPath: keyPath, withStatusCode: statusCode)
    
        return responseDescriptor
    }
    
    class func createUsuarioDescriptor() -> RKResponseDescriptor {
        
        let mapping = ONMapping.createUsuarioMapping()
        let method = RKRequestMethod.POST
        let pathPattern = "usuarios"
        let keyPath: NSString! = nil
        let statusCode = RKStatusCodeIndexSetForClass(UInt(201))
        
        let responseDescriptor = responseDescriptorWithMapping(mapping, withMethod: method, withPathPattern: pathPattern, withKeyPath: keyPath, withStatusCode: statusCode)
        
        return responseDescriptor
    }
    
    class func deleteSessionDescriptor() -> RKResponseDescriptor {
        
        let mapping = RKObjectMapping()
        let method = RKRequestMethod.DELETE
        let pathPattern = "sessions"
        let keyPath: NSString! = nil
        let statusCode = RKStatusCodeIndexSetForClass(UInt(201))
        
        let responseDescriptor = responseDescriptorWithMapping(mapping, withMethod: method, withPathPattern: pathPattern, withKeyPath: keyPath, withStatusCode: statusCode)
        
        return responseDescriptor
    }
    
    class func createDefaultErrorDescriptor() -> RKResponseDescriptor {
        
        let mapping = ONMapping.createDefaultErrorMapping()
        let method = RKRequestMethod.Any
        let statusCode = RKStatusCodeIndexSetForClass(UInt(RKStatusCodeClassClientError))
        
        let responseDescriptor = responseDescriptorWithMapping(mapping, withMethod: method, withPathPattern: nil, withKeyPath: nil, withStatusCode: statusCode)
        
        return responseDescriptor
    }
    
    class func responseDescriptorWithMapping(mapping: RKMapping, withMethod method: RKRequestMethod, withPathPattern pathPattern: String!, withKeyPath keyPath: String!, withStatusCode statusCode: NSIndexSet) -> RKResponseDescriptor {
        
        let responseDescriptor = RKResponseDescriptor(mapping: mapping, method: method, pathPattern: pathPattern, keyPath: keyPath, statusCodes: statusCode)
        
        return responseDescriptor
    }
    
    class func all() -> NSArray {
        return [createSessionDescriptor(), deleteSessionDescriptor(), createUsuarioDescriptor()]
    }
}
