//
//  ViewController.swift
//  ServicosGerais
//
//  Created by Augustus Costa on 07/11/14.
//  Copyright (c) 2014 Augustus Costa. All rights reserved.
//

import UIKit

class LoginViewController: FXFormViewController {
    
    override func awakeFromNib() {
        
        formController.form = ONCreateSessionForm()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkLogin()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        self.checkLogin()
    }
    
    func entrarForm(cell: FXFormFieldCellProtocol) {
        let form = cell.field.form as ONCreateSessionForm
        
        
        let erros : NSArray = form.isValid()
        
        if(erros.count > 0){
            var message : String = ""
            for (erro) in erros{
                message = message.stringByAppendingString(erro as String)
                message = message.stringByAppendingString("")
                UIAlertView(title: "Erro na validação dos campos", message: (erro as String), delegate: nil, cancelButtonTitle: "OK").show()
                return
            }
        }
        
        self.login(form.email, password: form.password)

    }
    
    func entrarComFacebookForm(cell: FXFormFieldCellProtocol) {
        let form = cell.field.form as ONCreateSessionForm
        
        //FACEBOOK THINGS

    }
    
    func criarContaFormForm(cell: FXFormFieldCellProtocol) {
        let form = cell.field.form as ONCreateSessionForm
        self.performSegueWithIdentifier("signupSegue", sender: self)
    }
    
    func checkLogin(){
        let managedContext:NSManagedObjectContext = RKObjectManager.sharedManager().managedObjectStore.persistentStoreManagedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "ONSession")
        let fetchResults: NSArray! = managedContext.executeFetchRequest(fetchRequest, error: nil)
        if  (fetchResults != nil && fetchResults.count > 0) {
            self.enter()
        }
        
    }
    
    func enter(){
        self.performSegueWithIdentifier("mainSegue", sender: self)
    }

    
    func login(email:String!, password:String!){
        
      let request:ONRequest = ONRequestFactory().createSessionRequest(email, password: password, notificationId: NSUserDefaults.standardUserDefaults().stringForKey("token"))
        
        RKObjectManager.sharedManager().postObject(nil, path:request.path, parameters:request.params,
            success:{ operation, mappingResult in
               self.checkLogin()
            },
            failure:{ operation, error in
                println("Login request failed with error: " + error!.localizedDescription)
                var erroMessage : NSString = error!.localizedRecoverySuggestion!
                let alertController = UIAlertController(title: "Acesso negado", message:erroMessage, preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "Voltar", style: UIAlertActionStyle.Default,handler: nil))
                
                self.presentViewController(alertController, animated: true, completion: nil)
                
            }
        )
        
    }

}

