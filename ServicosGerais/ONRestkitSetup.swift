//
//  ONRestkitSetup.swift
//  ServicosGerais
//
//  Created by Augustus Costa on 11/11/14.
//  Copyright (c) 2014 Augustus Costa. All rights reserved.
//

import Foundation


var objectStore: RKManagedObjectStore = RKManagedObjectStore()

func startRestkitWithBaseURL(baseURL: NSURL) {
    
    
    RKLogConfigureFromEnvironment()
    
    configureRestKitWithBaseURL(baseURL)
    
    setupResponseDescriptors()
}

func configureRestKitWithBaseURL(baseURL: NSURL) {
    
    RKObjectManager.setSharedManager(RKObjectManager(baseURL: baseURL))
    
    objectStore = RKManagedObjectStore(managedObjectModel: managedObjectModel())
    
    let dataPath = RKApplicationDataDirectory() + "servicosgerais.sqlite"
    
    objectStore.addSQLitePersistentStoreAtPath(dataPath, fromSeedDatabaseAtPath: nil, withConfiguration: nil, options: optionsForSqliteStore(), error: nil)
    
    objectStore.createManagedObjectContexts()
    
    objectStore.managedObjectCache = RKInMemoryManagedObjectCache(managedObjectContext: objectStore.persistentStoreManagedObjectContext)
    
    RKObjectManager.sharedManager().managedObjectStore = objectStore
    
}

func bundleName() -> NSString {
    
    let bundle = NSBundle.mainBundle()
    
    let info = bundle.infoDictionary! as NSDictionary
    
    let bundleName = info["CFBundleName"] as NSString
    
    return bundleName
}

func setupResponseDescriptors() {
    
    RKObjectManager.sharedManager().addResponseDescriptorsFromArray(ONResponseDescriptor.all())
    
}

func managedObjectModel() -> NSManagedObjectModel {
    
    return NSManagedObjectModel.mergedModelFromBundles(nil)!
    
}

func optionsForSqliteStore() -> NSDictionary {
    
    return [
        NSInferMappingModelAutomaticallyOption: true,
        NSMigratePersistentStoresAutomaticallyOption: true
    ];
    
}

