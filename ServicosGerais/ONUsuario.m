//
//  ONUsuario.m
//  ServicosGerais
//
//  Created by Augustus Costa on 26/01/15.
//  Copyright (c) 2015 Augustus Costa. All rights reserved.
//

#import "ONUsuario.h"


@implementation ONUsuario

@dynamic avatar_url;
@dynamic avatar_url_medium;
@dynamic avatar_url_thumb;
@dynamic email;
@dynamic facebook_avatar_url;
@dynamic identifier;
@dynamic nome;
@dynamic password;
@dynamic telefone;

@end
