//
//  MainViewController.swift
//  ServicosGerais
//
//  Created by Augustus Costa on 17/11/14.
//  Copyright (c) 2014 Augustus Costa. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logoutButtonClick(sender: UIButton) {
        self.logout();
    }
    
    func logout(){
        
        let session:ONSession = self.getActiveSession()
        let request:ONRequest = ONRequestFactory().deleteSessionRequest(session.identifier)
        
        RKObjectManager.sharedManager().deleteObject(nil, path:request.path, parameters:request.params,
            success:{ operation, mappingResult in
                self.deleteSession(session)
                self.dismissViewControllerAnimated(true, completion: nil)
                
            },
            failure:{ operation, error in
                println("Logout request failed with error: " + error!.localizedDescription)
                
            }
        )
    }
    
    func deleteSession(session:ONSession){
        let managedContext:NSManagedObjectContext = RKObjectManager.sharedManager().managedObjectStore.persistentStoreManagedObjectContext
        managedContext.deleteObject(session)
        managedContext.save(nil)
    }
    
    func getActiveSession()->ONSession{
        let managedContext:NSManagedObjectContext = RKObjectManager.sharedManager().managedObjectStore.persistentStoreManagedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "ONSession")
        let fetchResults:NSArray = managedContext.executeFetchRequest(fetchRequest, error: nil)!
        return fetchResults[0] as ONSession
    }
    
}