//
//  ONRequest.swift
//  ServicosGerais
//
//  Created by Augustus Costa on 11/11/14.
//  Copyright (c) 2014 Augustus Costa. All rights reserved.
//

import UIKit

class ONRequest: NSObject {
    
    var path: NSString!
    var params: NSMutableDictionary!
    
    func addParam(name:String, value:String){
        getParams().setObject(value, forKey: name)
    }
    
    func getParams()->NSMutableDictionary{
        if(params == nil){
            params = NSMutableDictionary()
        }
        return params
    }
    
}