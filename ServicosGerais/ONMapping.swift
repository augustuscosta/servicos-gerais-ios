//
//  ONMapping.swift
//  ServicosGerais
//
//  Created by Augustus Costa on 11/11/14.
//  Copyright (c) 2014 Augustus Costa. All rights reserved.
//

import UIKit

class ONMapping: NSObject {
    
    class func createSessionMapping() -> RKEntityMapping {
        let mappingDictionary = ["id":"identifier", "email":"email", "password":"password", "token":"token", "so":"so"]
        
        let identificationAttributes = ["identifier"]
        
        let entityMapping = mappingForEntity("ONSession", withMappingDictionary: mappingDictionary, withIdentificationAttributes: identificationAttributes)
        
        
        return entityMapping
    }
    
    class func createUsuarioMapping() -> RKEntityMapping {
        let mappingDictionary = ["id":"identifier",
            "nome":"nome",
            "email":"email",
            "password":"password",
            "telefone":"telefone",
            "avatar_url":"avatar_url",
            "avatar_url_medium":"avatar_url_medium",
            "avatar_url_thumb":"avatar_url_thumb",
            "facebook_avatar_url":"facebook_avatar_url"]
        
        let identificationAttributes = ["identifier"]
        
        let entityMapping = mappingForEntity("ONUsuario", withMappingDictionary: mappingDictionary, withIdentificationAttributes: identificationAttributes)
        
        
        return entityMapping
    }

    
    class func createDefaultErrorMapping() -> RKObjectMapping {
        let mappingDictionary = ["":"userInfo"]
        let errorMapping = RKObjectMapping(forClass: RKErrorMessage.self)
        errorMapping.addAttributeMappingsFromDictionary(mappingDictionary)
        return errorMapping
    }
    
    
    class func mappingForEntity(entity: NSString, withMappingDictionary mappingDictionary: NSDictionary, withIdentificationAttributes identificationAttributes: NSArray) -> RKEntityMapping {
        
        let entityMapping = RKEntityMapping(forEntityForName: entity, inManagedObjectStore: RKManagedObjectStore.defaultStore())
        
        entityMapping.addAttributeMappingsFromDictionary(mappingDictionary)
        
        entityMapping.identificationAttributes = identificationAttributes
        
        return entityMapping
    }
    
}
