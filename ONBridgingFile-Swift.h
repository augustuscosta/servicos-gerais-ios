//
//  ONBridingFile-Swift.h
//  ServicosGerais
//
//  Created by Augustus Costa on 04/11/14.
//  Copyright (c) 2014 Augustus Costa. All rights reserved.
//

#ifndef ServicosGerais_ONBridingFile_Swift_h
#define ServicosGerais_ONBridingFile_Swift_h

#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>


//MODEL IMPORTS

#import "ONSession.h"
#import "ONUsuario.h"


//FX Foms lib

#import "FXForms.h"

#endif
